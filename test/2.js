const response = {
	categories: {
		"Аксессуары": [
			"Рюкзаки",
			"Сумки",
			"Часы наручные"
		],
		"Белье": [
			"Боди",
			"Бордшорты",
			"Бюстгальтеры",
			"Бюстье",
			"Колготки",
			"Лифы для купальника",
			"Монокини",
			"Носки",
			"Плавки",
			"Пояса для чулок",
			"Раздельные купальники",
			"Слитные купальники",
			"Трусы"
		],
		"Белье для малышей": [
			"Носки для малышей"
		],
		"Красота": [
			"Щетки косметические"
		],
		"Одежда": [
			"Блузки",
			"Блузки-боди",
			"Бомберы",
			"Брюки",
			"Ветровки",
			"Водолазки",
			"Джеггинсы",
			"Джемперы",
			"Джинсы",
			"Жакеты",
			"Жилеты",
			"Капри",
			"Кардиганы",
			"Комбинезоны",
			"Костюмы",
			"Косухи",
			"Кофты",
			"Куртки",
			"Леггинсы",
			"Лонгсливы",
			"Ночные сорочки",
			"Пальто",
			"Парки",
			"Пиджаки",
			"Пижамы",
			"Платья",
			"Плащи",
			"Пуловеры",
			"Пуховики",
			"Рубашки",
			"Сарафаны",
			"Свитеры",
			"Свитшоты",
			"Толстовки",
			"Топы",
			"Туники",
			"Футболки",
			"Футболки-поло",
			"Халаты домашние",
			"Худи",
			"Шорты",
			"Юбки"
		]
	}
}

const dataArray = Object.entries(response.categories)

let parent_id = 0

function generateChildrenCategories(childrenElement) {
	let iterationValue = 1

	return childrenElement.map((child) => {
		const children_id = (100 * parent_id) + iterationValue
		iterationValue ++

		return {
			"children_value": children_id,
			"children_label": child,
			"parent_key": parent_id
		}
	})
}

export const resultData = dataArray.map(([parentCategory, childrenCategory]) => {
	++parent_id

	const childCategories = generateChildrenCategories(childrenCategory)
	// не нравится мне, что внутри цикла еще один цикл. Вроде бы сложность алгоритма O(n**2))))
	// лучше не придумал на данный момент

	return {
		parent_value: parent_id,
		"parent_label": parentCategory,
		children: [...childCategories]
	}
})
